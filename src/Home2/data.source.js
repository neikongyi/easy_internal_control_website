import React from 'react';
import c1 from '../assets/content0/1.png';
import c2 from '../assets/content0/2.png';
import c3 from '../assets/content0/3.png';
import c4 from '../assets/content0/4.png';
import c5 from '../assets/content0/5.png';
import c6 from '../assets/content0/6.png';
import c7 from '../assets/content0/7.png';
import c8 from '../assets/content0/8.png';


import school from '../assets/school.png';
import s1 from '../assets/school2/1.jpg';
import s2 from '../assets/school2/2.png';
import s3 from '../assets/school2/3.png';
import s4 from '../assets/school2/4.png';
import s5 from '../assets/school2/5.png';
import s6 from '../assets/school2/6.png';
import s7 from '../assets/school2/7.png';


import school1 from '../assets/schools/9.jpg';
import school2 from '../assets/schools/10.png';
import school3 from '../assets/schools/11.png';
import school4 from '../assets/schools/12.png';
import school5 from '../assets/schools/13.png';
import school6 from '../assets/schools/13.png';
import bigLogo from '../assets/bigLogo.png';
import logo from '../assets/logo.png';
import flogo from '../assets/flogo.png';

import a1 from '../assets/advantage/1.png';
import a2 from '../assets/advantage/2.png';
import a3 from '../assets/advantage/3.png';
import a4 from '../assets/advantage/4.png';
import a5 from '../assets/advantage/5.png';
import a6 from '../assets/advantage/6.png';

export const Nav00DataSource = {
  wrapper: { className: 'header0 home-page-wrapper k14ctcbqfbi-editor_css' },
  page: { className: 'home-page' },
  logo: {
    className: 'header0-logo',
    children: logo,
  },
  Menu: {
    className: 'header0-menu',
    children: [
      {
        name: 'item0',
        className: 'header0-item',
        children: {
          href: '#Banner0_1',
          children: [
            {
              children: '首页',
              name: 'text',
            },
          ],
        },
      },
      {
        name: 'item1',
        className: 'header0-item',
        children: {
          href: '#Content0_0',
          children: [
            {
              children: '产品与服务',
              name: 'text',
            },
          ],
        },
      },
      {
        name: 'item2',
        className: 'header0-item',
        children: {
          href: '#Content3_0',
          children: [
            {
              children: '我们的优势',
              name: 'text',
            },
          ],
        },
      },
      {
        name: 'item3',
        className: 'header0-item',
        children: {
          href: '#Content5_0',
          children: [
            {
              children: '客户案例',
              name: 'text',
            },
          ],
        },
      },
    ],
  },
  mobileMenu: { className: 'header0-mobile-menu' },
};
export const Banner01DataSource = {
  wrapper: { className: 'banner0 k14ctkdhkz-editor_css' },
  textWrapper: { className: 'banner0-text-wrapper k14cttzd7hl-editor_css' },
  title: {
    className: 'banner0-title',
    children: bigLogo,
  },
  content: {

  },
  button: {

  },
};
export const Content00DataSource = {
  wrapper: { className: 'home-page-wrapper content0-wrapper' },
  page: { className: 'home-page content0' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [{ name: 'title', children: '全面满足单位内控信息化管理需求' }],
  },
  childWrapper: {
    className: 'content0-block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c1,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '预算管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c2,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '收支管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c3,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '采购管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c4,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '合同管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c5,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '项目管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c6,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '资产管理',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c7,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '决策支持',
            },
          ],
        },
      },
      {
        name: 'block0',
        className: 'content0-block',
        md: 6,
        xs: 12,
        gutter: 0,
        children: {
          className: 'content0-block-item',
          children: [
            {
              name: 'image',
              className: 'content0-block-icon',
              children: c8,
            },
            {
              name: 'title',
              className: 'content0-block-title',
              children: '单位层面内控',
            },
          ],
        },
      },
    ],
  },
};
export const Content30DataSource = {
  wrapper: {
    className: 'home-page-wrapper content3-wrapper k14cyw7bt8p-editor_css',
  },
  page: { className: 'home-page content3' },
  OverPack: { playScale: 0.3 },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      {
        name: 'title',
        children: (
          <>
            <p>我们的优势</p>
          </>
        ),
        className: 'title-h1',
      },
      {
        name: 'content',
        className: 'title-content k14dewgcpna-editor_css',
        children: '基于阿里云强大的基础资源',
      },
    ],
  },
  block: {
    className: 'content3-block-wrapper',
    children: [
      {
        name: 'block0',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children: a1,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dfh68zq-editor_css',
            children: (
              <>
                <p>行业内控信息化成功案例第一</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  内控易拥有数百家成功应用的行业客户，包括教育、街道、科技、人社及各类机关单位，已有单位成功应用，基于用户反馈持续迭代打磨精品。
                </p>
              </>
            ),
          },
        },
      },
      {
        name: 'block1',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon k14eioaqp7-editor_css',
            children: a2,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dgrzwvu-editor_css',
            children: (
              <>
                <p>全球领先的内控产品及技术</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  内控易使用全新一代企业互联网技术设计，能够更快、更好的满足单位业务管理的个性化需求并提供智能化服务。
                </p>
              </>
            ),
          },
        },
      },
      {
        name: 'block2',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children: a3,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dl2basjd-editor_css',
            children: (
              <>
                <p>智能化的行业内控应用</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  采取智能化应用技术，将行业内控最佳管理实践融入系统规则库
                  ，在防范风险的基础上最大程度减少人工操作的繁琐工作。
                </p>
              </>
            ),
          },
        },
      },
      {
        name: 'block3',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children: a4,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dlykq759-editor_css',
            children: (
              <>
                <p>支付宝级的安全保障</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  采取符合等保三级要求的云安全防护，从系统、通讯、网络、数据、身份、信息安全等6个维度确保。
                </p>
              </>
            ),
          },
        },
      },
      {
        name: 'block4',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children: a5,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dmljg92j-editor_css',
            children: (
              <>
                <p>简单易用的用户体验</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  互联网产品设计团队出品，为用户提供淘宝购物式体验，操作简单、一分钟就学会。
                </p>
              </>
            ),
          },
        },
      },
      {
        name: 'block5',
        className: 'content3-block',
        md: 8,
        xs: 24,
        children: {
          icon: {
            className: 'content3-icon',
            children: a6,
          },
          textWrapper: { className: 'content3-text' },
          title: {
            className: 'content3-title k14dn80afzk-editor_css',
            children: (
              <>
                <p>高性价比的一站式服务</p>
              </>
            ),
          },
          content: {
            className: 'content3-content',
            children: (
              <>
                <p>
                  颠覆传统软件动辄上百万的初期投入风险，采取开箱即用、按效果付费的一站式云服务模式保障客户投资。
                </p>
              </>
            ),
          },
        },
      },
    ],
  },
};
export const Content50DataSource = {
  wrapper: {
    className: 'home-page-wrapper content5-wrapper k14d0kvpoxn-editor_css',
  },
  page: { className: 'home-page content5' },
  OverPack: { playScale: 0.3, className: '' },
  titleWrapper: {
    className: 'title-wrapper',
    children: [
      { name: 'title', children: '客户案例', className: 'title-h1' },
      {
        name: 'content',
        className: 'title-content k14nh2al3j-editor_css',
        children: '在这里用一段话介绍服务的案例情况',
      },
    ],
  },
  tabWrapper: {
    className: 'tab-wrapper'
  },
  block: {
    className: 'content5-img-wrapper',
    gutter: 16,
    children: [
      {
        name: 'block0',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: s1,
            className: 'k14k6g8ammf-editor_css',
          },
          content: {
            children: (
              <>
                <p>北京市八一学校</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block1',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: s2,
            className: 'k14khd7yhp-editor_css',
          },
          content: {
            children: (
              <>
                <p>北京市十一学校</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block2',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: s3,
            className: 'k14ko6ubzd8-editor_css',
          },
          content: {
            children: (
              <>
                <p>人民大学附属中学</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block3',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: s4,
            className: 'k14l9cjrdmr-editor_css',
          },
          content: {
            children: (
              <>
                <p>清华大学附属中学</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block4',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children:
              s5,
            className: 'k14nahyb88s-editor_css',
          },
          content: {
            children: (
              <>
                <p>北京市海淀区中关村第三小学</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block5',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children:
              s6,
            className: 'k14n9qv2wgt-editor_css',
          },
          content: {
            children: (
              <>
                <p>北京市海淀区中关村第一小学</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block6',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: s7,
            className: 'k14n4r0fyu-editor_css',
          },
          content: {
            children: (
              <>
                <p>成都市茶店子小学校</p>
              </>
            ),
          },
        },
      },
      {
        name: 'block7',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: { children: school, className: 'k14n7geym3r-editor_css' },
          content: {
            children: (
              <>
                <p>北京市海淀区立新幼儿园</p>
              </>
            ),
          },
        },
      },
    ],
  },
  block2: {
    className: 'content5-img-wrapper',
    gutter: 16,
    children: [
      {
        name: 'block0',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school1,
            className: 'k14k6g8ammf-editor_css',

          },
          content: {
            children: '北京市海淀区教育委员会',
          },
        },
      },
      {
        name: 'block1',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school3,
            className: 'k14khd7yhp-editor_css',
          },
          content: {
            children: '成都市教育科学研究院',
          },
        },
      },
      {
        name: 'block2',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school2,
            className: 'k14ko6ubzd8-editor_css',
          },
          content: {
            children: '北京市海淀区教委会计核算中心',
          },
        },
      },
      {
        name: 'block3',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school4,
            className: 'k14l9cjrdmr-editor_css',
          },
          content: {
            children: '北京市海淀区教育招生和考试中心',
          },
        },
      },
      {
        name: 'block4',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children:
              school5,
            className: 'k14nahyb88s-editor_css',
          },
          content: {
            children: '中关村学区管理中心',
          },
        },
      },
      {
        name: 'block5',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school6,
            className: 'k14n9qv2wgt-editor_css',
          },
          content: {
            children: '中小学体育卫生中心',
          },
        },
      },
      {
        name: 'block6',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school6,
            className: 'k14n4r0fyu-editor_css',
          },
          content: {
            children: '花园路学区管理中心',
          },
        },
      },
      {
        name: 'block7',
        className: 'block',
        md: 6,
        xs: 24,
        children: {
          wrapper: { className: 'content5-block-content' },
          img: {
            children: school6,
            className: 'k14n7geym3r-editor_css'
          },
          content: {
            children: '海淀区后勤管理中心',
          },
        },
      },
    ],
  },
};
export const Footer10DataSource = {
  wrapper: { className: 'home-page-wrapper footer1-wrapper' },
  OverPack: { className: 'footer1', playScale: 0.2 },
  block: {
    className: 'home-page',
    gutter: 0,
    children: [
      {
        name: 'block0',
        xs: 24,
        md: 6,
        className: 'block',
        title: {
          className: 'logo',
          children: flogo,
        },
        childWrapper: {
          className: 'slogan',
          children: [
            {
              name: 'content0',
              children: (
                <>
                  <p>做最好用的行业内控管理云平台</p>
                </>
              ),
              className: 'k14jvv8q8i-editor_css',
            },
          ],
        },
      },
      {
        name: 'block1',
        xs: 24,
        md: 6,
        className: 'block',
        title: {
          children: (
            <>
              <p>合作伙伴</p>
            </>
          ),
        },
        childWrapper: {
          children: [
            {
              name: 'link0',
              href: '#',
              children: (
                <>
                  <p>阿里云</p>
                </>
              ),
            },
            {
              name: 'link1',
              href: '#',
              children: (
                <>
                  <p>企业微信</p>
                </>
              ),
            },
            {
              name: 'link2',
              href: '#',
              children: (
                <>
                  <p>钉钉</p>
                </>
              ),
            },
            {
              name: 'link3',
              href: '#',
              children: (
                <>
                  <p>用友G-U8</p>
                </>
              ),
            },
            {
              name: 'link~k14g1ehw82s',
              href: '#',
              children: (
                <>
                  <p>畅捷通G6</p>
                </>
              ),
            },
          ],
        },
      },
      {
        name: 'block2',
        xs: 24,
        md: 6,
        className: 'block',
        title: { children: '关于' },
        childWrapper: {
          children: [
            {
              href: '#',
              name: 'link0',
              children: (
                <>
                  <p>FAQ</p>
                </>
              ),
            },
            { href: '#', name: 'link1', children: '联系我们' },
          ],
        },
      },
      // {
      //   name: 'block3',
      //   xs: 24,
      //   md: 6,
      //   className: 'block k14g1xwvwns-editor_css',
      //   title: { children: '资源' },
      //   childWrapper: {
      //     children: [
      //       { href: '#', name: 'link0', children: 'Ant Design' },
      //       { href: '#', name: 'link1', children: 'Ant Motion' },
      //     ],
      //   },
      // },
    ],
  },
  copyrightWrapper: { className: 'copyright-wrapper' },
  copyrightPage: { className: 'home-page' },
  copyright: {
    className: 'copyright',
    children: (
      <>
        <div>
          <a href="https://beian.miit.gov.cn/" target="_blank">京ICP备18019044号</a>
          <br/>
          ©2018 by <a>内控易</a> All Rights
          Reserved
        </div>
      </>
    ),
  },
};
