import React from 'react';
import { Button, Icon } from 'antd';
import QueueAnim from 'rc-queue-anim';
import TweenOne from 'rc-tween-one';
import bg from '../assets/bg.jpg';

class Banner extends React.PureComponent {
  render() {
    const { ...currentProps } = this.props;
    const { dataSource } = currentProps;
    delete currentProps.dataSource;
    delete currentProps.isMobile;
    return (
      <div
          {...currentProps} {...dataSource.wrapper}
          style={{ backgroundImage: `url(${bg})`}}
      >
        <QueueAnim
          key="QueueAnim"
          type={['bottom', 'top']}
          delay={200}
          {...dataSource.textWrapper}
        >
          <div key="title" {...dataSource.title}>
              <img src={dataSource.title.children} width="100%" alt="img" />
          </div>
          <div key="content" {...dataSource.content}>
            {dataSource.content.children}
          </div>
        </QueueAnim>
        <TweenOne
          animation={{
            y: '-=20',
            yoyo: true,
            repeat: -1,
            duration: 1000,
          }}
          className="banner0-icon"
          key="icon"
        >
            <a href="#Content0_0">
                <Icon type="down" />
            </a>
        </TweenOne>
      </div>
    );
  }
}
export default Banner;
