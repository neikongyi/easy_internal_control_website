export default {
    plugins: [
        [
            'umi-plugin-react', {
            antd: true,
        }
        ],
        ['@umijs/plugin-prerender', {}]
    ],
    exportStatic: {
        // htmlSuffix: true,
        dynamicRoot: true,
    },
    "theme": {
        "primary-color": "#1bc0cb",
    },
    ssr: true,
}
